from dataclasses import dataclass, field
from typing import Any, Dict, List, Optional

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db.models.query_utils import Q

from ..models import Letter
from .base import BaseMessageBackend

__all__ = 'EmailMessageBackend',


class EmailMessageBackend(BaseMessageBackend):
    """
    Email backend provides sending emails.
    """

    # config_form_path = 'kld_lettre.forms.EmailConfigForm'
    recipients_resolver_path = 'kld_lettre.recipients.EmailRecipientsResolver'
    template_fields = ['subject', 'content', 'plain_text']
    template_form_path = 'kld_lettre.forms.EmailTemplateForm'
    template_renderer_path= 'kld_lettre.renderers.Jinja2Renderer'
    template_data_generator_path = (
        'kld_lettre.generators.template_data.EmailTemplateDataGenerator'
    )

    def get_default_sender(self):
        if 'des' in settings.INSTALLED_APPS:
            from des.models import DynamicEmailConfiguration
            return DynamicEmailConfiguration.get_solo().from_email
        return settings.DEFAULT_FROM_EMAIL

    def get_letter_data(
        self,
        *,
        channel: str,
        event: str,
        context: Dict[str, Any],
        **kwargs
    ) -> Dict:
        letter_data = super().get_letter_data(
            channel=channel,
            event=event,
            context=context,
            **kwargs
        )
        if not letter_data.get('sender'):
            letter_data['sender'] = self.get_default_sender()
        return letter_data

    def send(self, *, letter: Letter) -> bool:
        data = letter.data
        message = EmailMultiAlternatives(
            subject=data.get('subject'),
            body=data.get('plain_text'),
            from_email=data.get('sender'),
            to=[
                item['email']
                for item in data.get('recipients', [])
                if 'email' in item
            ],
        )
        message.attach_alternative(data.get('content'), "text/html")
        message.send()
        return True
