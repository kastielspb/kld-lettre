from typing import Any, Callable, Dict, List, Optional, Tuple

from django.utils.module_loading import import_string
from django.utils.translation import pgettext_lazy

from ..models import Template, Letter
from ..services import letter_service, template_service

__all__ = 'BaseMessageBackend',


class BaseMessageBackend:
    """
    Base backend that create letter, send it and complite letter.
    Instance creates for channel at the time of use.
    All other backends must inherites from this backend. Every backend must
    implement method `send`.

    Arguments:
        config_form_path (str): path to default config admin form.
        recipients_resolver_path (str): path to recipients_resolver wrapper, \
        that collect recipients info for specific backend.
    """

    config_form_path = 'kld_lettre.forms.BaseConfigForm'
    recipients_resolver_path = 'kld_lettre.recipients.BaseRecipientsResolver'
    template_fields = []
    template_form_path = 'kld_lettre.forms.BaseTemplateForm'
    template_renderer_path = 'kld_lettre.renderers.Jinja2Renderer'
    template_data_generator_path = (
        'kld_lettre.generators.template_data.BaseTemplateDataGenerator'
    )

    def __init__(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])

        self.recipients_resolver = self.get_recipients_resolver()
        self.template_renderer = self.get_template_renderer()
        self.template_data_generator = self.get_template_data_generator()

    def get_template_fields(self):
        return self.template_fields

    def get_recipients_resolver(self):
        klass = import_string(self.recipients_resolver_path)
        return klass()

    def get_template_renderer(self) -> Callable[[Any, Dict], Dict]:
        klass = import_string(self.template_renderer_path)
        return klass()

    def get_template_data_generator(self) -> Callable[[Dict], Dict]:
        klass = import_string(self.template_data_generator_path)
        return klass()

    def get_template(self, channel: str, event: str) -> Template:
        template = (
            Template
            .objects
            .filter(channel=channel, event=event)
            .first()
        )
        if template:
            return template
        raise (
            KeyError(
                pgettext_lazy(
                    'messages',
                    'No MailTemplate for channel "{}" and event "{}".'
                )
                .format(channel, event)
            )
        )

    def send(self, *, letter: Letter) -> bool:
        raise NotImplementedError

    def send_letter(
        self,
        *,
        channel: str,
        event: str,
        context: Dict[str, Any],
        recipients: List[Dict[str, Any]],
        letter: Letter = None,
        fail_silently=False,
        **kwargs
    ) -> Tuple[bool, Letter]:
        try:
            if letter is None:
                letter = letter_service.create(
                    **self.get_letter_kwargs(
                        channel=channel,
                        event=event,
                        context=context,
                        recipients=self.recipients_resolver(recipients),
                        **kwargs
                    )
                )
            is_sent = self.send(letter=letter)
            letter_service.complite(letter=letter)
            return is_sent, letter
        except Exception as exc:
            # TODO: add logger like in moddule logging
            letter_service.complite(letter=letter)
            if fail_silently:
                return False, letter
            raise exc

    def get_letter_kwargs(
        self,
        *,
        channel: str,
        event: str,
        context: Dict[str, Any],
        recipients: List[Dict[str, Any]],
        **kwargs
    ) -> Dict:
        template = self.get_template(channel=channel, event=event)
        return {
            'channel': channel,
            'event': event,
            'data': self.get_letter_data(
                channel=channel,
                event=event,
                template=template,
                context=context,
                recipients=recipients,
                **kwargs
            )
        }

    def get_letter_data(
        self,
        *,
        channel: str,
        event: str,
        template: Template,
        context: Dict[str, Any],
        recipients: List[Dict[str, Any]],
        **kwargs
    ) -> Dict:
        result, errors = template_service.render_template(
            template=template,
            fields=self.get_template_fields(),
            renderer=self.template_renderer,
            context=context,
            language=kwargs.get('language'),
        )
        if errors:
            raise Exception(errors)

        data = {'recipients': recipients}
        data.update(result)
        return data
