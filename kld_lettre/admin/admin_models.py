from django.contrib import admin
from django.contrib.messages import constants as message_const
from django.utils.html import mark_safe
from django.utils.translation import pgettext, pgettext_lazy

from pxd_lingua.admin import TranslationsInlineAdmin

from ..forms import AddConfigForm, AddTemplateForm
from ..models import *
from ..services import admin_service, sender_service, template_service
from .formsets import EntangledFormSet
from .utils import prettify_json, render_html_table

__all__ = 'TemplateAdmin',


class JsonDataAdminMixin:

    def json_data(self, obj):
        return mark_safe(prettify_json(getattr(obj, 'data', {})))
    json_data.short_description = pgettext_lazy("kld_lettre", "Json data")


@admin.register(ChannelConfig)
class ChannelConfigAdmin(JsonDataAdminMixin, admin.ModelAdmin):
    add_form = AddConfigForm
    readonly_fields = 'json_data', 'created', 'updated'

    def get_form(self, request, obj=None, **kwargs):
        defaults = {
            'form': (
                self.add_form
                if obj is None
                else (
                    admin_service
                    .get_configurator(channel=obj.channel)
                    .config_form_class
                )
            )
        }
        defaults.update(kwargs)
        return super().get_form(request, obj, **defaults)


class TemplateTranslationsInlineAdmin(TranslationsInlineAdmin):
    formset = EntangledFormSet
    model = TemplateTranslation

    def get_ent_form(self, parent_obj):
        configurator = admin_service.get_configurator(
            channel=parent_obj.channel
        )
        BaseForm = configurator.template_form_class
        trans_untangled_fields = list(
            set(getattr(BaseForm.Meta, 'untangled_fields', []))
            - set(['channel', 'event'])
            | set(['language'])
        )

        class TransForm(BaseForm, self.form):

            class Meta(BaseForm.Meta):
                fields = 'language',
                model = self.model
                untangled_fields = trans_untangled_fields

            def get_backend(self):
                return (
                    sender_service
                    .get_backend(channel=parent_obj.channel)
                )

            def get_event(self, data):
                return parent_obj.event

        return TransForm

    def get_formset(self, request, obj=None, **kwargs):
        if obj:
            if not hasattr(self, '_ent_form'):
                self._ent_form = self.get_ent_form(obj)
            self.form = self._ent_form
        return super().get_formset(request, obj=obj, **kwargs)


@admin.register(Template)
class TemplateAdmin(JsonDataAdminMixin, admin.ModelAdmin):
    add_form = AddTemplateForm
    add_fieldsets = (None, {'fields': ['channel', 'event']}),
    inlines = TemplateTranslationsInlineAdmin,
    list_display = 'channel', 'event'
    list_filter= 'channel', 'event'
    readonly_fields = 'legend_table', 'json_data'

    def get_fieldsets(self, request, obj=None):
        self.inlines = self.__class__.inlines
        if obj is None:
            self.inlines = ()
            return self.add_fieldsets
        return super().get_fieldsets(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        defaults = {
            'form': (
                self.add_form
                if obj is None
                else (
                    admin_service
                    .get_configurator(channel=obj.channel)
                    .template_form_class
                )
            )
        }
        defaults.update(kwargs)
        return super().get_form(request, obj, **defaults)

    def legend_table(self, obj):
        if not getattr(obj, 'id', None):
            return '-'

        context_definition = (
            template_service.get_context_definition(event=obj.event)
        )
        data = [(
            pgettext("kld_lettre", "Title"),
            pgettext("kld_lettre", "Key"),
        )]
        for key, title in context_definition.items():
            data.append((str(title), key))
        return mark_safe(render_html_table(data))
    legend_table.short_description = pgettext_lazy("kld_lettre", "Legend")


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    exclude = 'letter',
    list_display = 'id', 'title', 'created'
    readonly_fields = 'created', 'updated'

    def get_queryset(self, request):
        return super().get_queryset(request).filter(letter__isnull=True)


class LogInline(admin.TabularInline):
    extra = 0
    model = Log
    readonly_fields = 'created', 'updated'


@admin.register(Letter)
class LetterAdmin(JsonDataAdminMixin, admin.ModelAdmin):
    actions = 'resend',
    inlines = LogInline,
    list_display = 'channel', 'event', 'status', 'created', 'updated'
    list_filter = 'channel', 'event', 'status'
    readonly_fields = 'json_data', 'created', 'updated'

    def resend(self, request, qs):
        result = sender_service.resend(letters=qs)
        if result['sent_count']:
            self.message_user(
                request=request,
                message=(
                    pgettext_lazy(
                        "kld_lettre",
                        "Successuly resent {} letters."
                    )
                    .format(result['sent_count'])
                ),
                level=message_const.SUCCESS
            )
        if result['error_ids']:
            self.message_user(
                request=request,
                message=(
                    pgettext_lazy(
                        "kld_lettre",
                        "Failed reseding {} letters with id: {}."
                    )
                    .format(
                        len(result['error_ids']),
                        ', '.join([str(pk) for pk in result['error_ids']])
                    )
                ),
                level=message_const.ERROR
            )
    resend.short_description = pgettext_lazy("kld_lettre", "Resend letters")
