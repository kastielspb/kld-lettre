from typing import Dict, List, Tuple
import json

__all__ = 'prettify_json', 'render_html_table'


def render_html_table(data: List[Tuple]) -> str:
    html = '<table style="border: 1px solid #ecf2f6;"><tbody>'
    for line in data:
        html += '<tr><th>'
        html += '</th><th>'.join(line)
        html += '</th></tr>'
    html += '</tbody></table>'
    return html


def prettify_json(data: Dict) -> str:
    return (
        ''.join([
            '<span style="white-space: pre-wrap">',
            json.dumps(data, ensure_ascii=False, indent=4),
            '</span>',
        ])
    )
