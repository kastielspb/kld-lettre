from django.forms import BaseInlineFormSet
from django.forms.formsets import DELETION_FIELD_NAME

__all__ = 'EntangledFormSet',


class EntangledFormSet(BaseInlineFormSet):

    def add_fields(self, form, index):
        super().add_fields(form, index)
        if self.can_delete:
            # Enable deleting for EntangledModelForm based.
            form._meta.untangled_fields.append(DELETION_FIELD_NAME)
