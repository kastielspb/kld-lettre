from typing import Union
from django.db import models
from django.utils.translation import pgettext_lazy
from django.db.models import JSONField

from pxd_lingua import create_translated_model

from .const import LETTER_STATUSES, settings

__all__ = (
    'ChannelConfig',
    'DateTimeModel',
    'Letter',
    'Log',
    'Template',
    'TemplateTranslation',
)


class DateTimeModel(models.Model):
    created = models.DateTimeField(
        pgettext_lazy('kld_lettre', 'Created'),
        auto_now_add=True
    )
    updated = models.DateTimeField(
        pgettext_lazy('kld_lettre', 'Updated'),
        auto_now=True
    )

    class Meta:
        abstract = True


class ChannelConfig(DateTimeModel):
    channel = models.CharField(
        pgettext_lazy('kld_lettre', 'Channel'),
        max_length=255,
        choices=settings.CHANNELS_CHOICES,
        unique=True
    )
    data = JSONField(pgettext_lazy('kld_lettre', 'Data'), default=dict)

    class Meta:
        verbose_name = pgettext_lazy('kld_lettre', 'Channel config')
        verbose_name_plural = pgettext_lazy('kld_lettre', 'Channel configs')

    def __str__(self) -> str:
        return self.get_channel_display()


class Template(DateTimeModel):
    channel = models.CharField(
        pgettext_lazy('kld_lettre', 'Channel'),
        max_length=255,
        choices=settings.CHANNELS_CHOICES
    )
    event = models.CharField(
        pgettext_lazy('kld_lettre', 'Event'),
        max_length=255,
        choices=settings.EVENT_CHOICES,
    )
    data = JSONField(pgettext_lazy('kld_lettre', 'Data'), default=dict)

    class Meta:
        unique_together = ('channel', 'event'),
        verbose_name = pgettext_lazy('kld_lettre', 'Template')
        verbose_name_plural = pgettext_lazy('kld_lettre', 'Templates')

    def __str__(self) -> str:
        return self.get_event_display()


class Letter(DateTimeModel):
    status = models.PositiveSmallIntegerField(
        pgettext_lazy('kld_lettre', 'Status'),
        choices=LETTER_STATUSES.choices,
        default=LETTER_STATUSES.draft
    )
    channel = models.CharField(
        pgettext_lazy('kld_lettre', 'Channel'),
        max_length=255,
        choices=settings.CHANNELS_CHOICES
    )
    event = models.CharField(
        pgettext_lazy('kld_lettre', 'Event'),
        max_length=255,
        choices=settings.EVENT_CHOICES,
    )
    data = JSONField(pgettext_lazy('kld_lettre', 'Data'), default=dict)

    class Meta:
        verbose_name = pgettext_lazy('kld_lettre', 'Letter')
        verbose_name_plural = pgettext_lazy('kld_lettre', 'Letters')


class Log(DateTimeModel):
    title = models.CharField(
        pgettext_lazy('kld_lettre', 'Title'),
        max_length=255
    )
    traceback = models.TextField(
        verbose_name=pgettext_lazy('kld_lettre', 'Traceback'),
        blank=True,
        null=True
    )
    letter = models.ForeignKey(
        Letter,
        verbose_name=pgettext_lazy('kld_lettre', 'Letter'),
        on_delete=models.CASCADE,
        related_name='logs',
        blank=True,
        null=True
    )

    class Meta:
        ordering = ('-id', )
        verbose_name = pgettext_lazy('kld_lettre', 'Log')
        verbose_name_plural = pgettext_lazy('kld_lettre', 'Logs')


TemplateTranslation = create_translated_model(
    Template,
    fields=('data', )
)

