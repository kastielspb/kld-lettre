from typing import Any, Dict, List, Tuple, Union

__all__ = 'BaseRecipientsResolver',


class BaseRecipientsResolver:
    fields: Union[List, Tuple] = None

    def __call__(
        self,
        data: Union[
            List[Dict[str, Any]],
            Tuple[Dict[str, Any]],
        ]
    ) -> Dict[str, Any]:
        if self.fields is None:
            return []

        recipients = []
        for item in data:
            info = {
                field: item.get(field)
                for field in self.fields
            }
            if info:
                recipients.append(info)
        return recipients


class EmailRecipientsResolver(BaseRecipientsResolver):
    fields = 'email',
