from typing import Dict

from .backends.base import BaseMessageBackend
from .models import ChannelConfig

__all__ = (
    'admin_configs',
    'message_backends',
)


admin_configs: Dict[str, ChannelConfig] = {}
message_backends: Dict[str, BaseMessageBackend] = {}
