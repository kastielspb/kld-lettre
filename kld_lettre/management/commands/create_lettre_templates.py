from django.core.management.base import BaseCommand
from django.db import transaction

from kld_lettre.services import template_service


class Command(BaseCommand):
    """Creates message templates that are not created."""

    help = ('Creates message templates that are not created.')

    @transaction.atomic
    def handle(self, *args, **options):
        for result in template_service.generate():
            self.stdout.write(
                self.style.SUCCESS(
                    'Created {} template(s) for section "{}".'
                    .format(result["count"], result["channel"])
                )
            )
