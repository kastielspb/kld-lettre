from typing import Any, Dict

__all__ = 'Jinja2PlugGenerator',


class Jinja2PlugGenerator:
    delimiter: str = '\n'

    def __call__(self, channel: str, event: str, data: Dict[str, Any]) -> str:
        return self.delimiter.join([
            f"{data[key]} : {{{{ {key} }}}}"
            for key in data
        ])
