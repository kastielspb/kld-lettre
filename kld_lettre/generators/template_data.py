from typing import Any, Dict, List, Tuple, Union

from ..const import settings

__all__ = 'BaseTemplateDataGenerator', 'EmailTemplateDataGenerator'

event_title_map = dict(settings.EVENT_CHOICES)


class BaseTemplateDataGenerator:

    def __call__(
        self,
        channel: str,
        event: str,
        fields: Union[List[str], Tuple[str]],
        plug: str,
    ) -> Dict[str, Any]:
        return {field: plug for field in fields}


class EmailTemplateDataGenerator(BaseTemplateDataGenerator):

    def __call__(
        self,
        channel: str,
        event: str,
        fields: Union[List[str], Tuple[str]],
        plug: str,
    ) -> Dict[str, Any]:
        data = {field: plug for field in fields}
        if 'subject' in fields:
            data['subject'] = str(event_title_map.get(event, '-'))
        return data
