from typing import Any, Dict, List, Tuple, Union

from django.utils.module_loading import import_string

from ..const import settings
from ..models import Template
from ..services import sender_service

__all__ = 'context_definition', 'generate', 'render_template'


def get_context_definition(event: str) -> Dict[str, Any]:
    return settings.EVENTS_CONTEXT.get(event, {})

def get_exists_events(channel: str):
    return (
        Template
        .objects
        .filter(channel=channel)
        .values_list('event', flat=True)
    )

def get_initial_text(data: Dict[str, Any], key: str) -> str:
    return data.get(key, '')

def render_template(
    *,
    template: Union[Dict, Template],
    fields: Union[List[str], Tuple[str]],
    renderer: object,
    context: Dict[str, Any],
    language: str = None,
) -> Tuple[Dict[str, str], Dict[str, Exception]]:
    errors = {}
    result = {}
    translation = (
        template.translations.by_language(language).first()
        if isinstance(template, Template)
        else None
    )
    instance = translation if translation else template
    data = getattr(instance, 'data', instance)
    for field in fields:
        try:
            result[field] = renderer(
                text=get_initial_text(data, field),
                context=context,
                language=language,
            )
        except Exception as exc:
            errors[field] = exc
    return result, errors

def get_plug_generator(renderer_path: str) -> object:
    path = settings.GENERATOR_RENDERERS.get(renderer_path)
    if path is None:
        return

    klass = import_string(path)
    return klass()

def generate() -> List[Dict[str, Any]]:
    results = []
    channel_title_map = dict(settings.CHANNELS_CHOICES)
    for channel, title in settings.CHANNELS_CHOICES:
        backend = sender_service.get_backend(channel=channel)
        plug_generator = get_plug_generator(backend.template_renderer_path)
        if plug_generator is None:
            continue

        to_create = []
        events = settings.CHANNEL_EVENTS.get(channel, [])
        exists = set(get_exists_events(channel=channel))
        for event in set(events) - exists:
            to_create.append(
                Template(
                    channel=channel,
                    event=event,
                    data=backend.template_data_generator(
                        channel=channel,
                        event=event,
                        fields=backend.get_template_fields(),
                        plug=plug_generator(
                            channel=channel,
                            event=event,
                            data=get_context_definition(event=event),
                        )
                    )
                )
            )
        created = Template.objects.bulk_create(to_create)
        results.append({
            'count': len(created),
            'channel': channel_title_map.get(channel, '-')
        })
    return results
