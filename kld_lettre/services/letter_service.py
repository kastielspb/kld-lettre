import sys
import traceback
from typing import Any, Dict, List, Optional

from ..const import LETTER_STATUSES
from ..models import Letter, Log

__all__ = 'complite', 'create', 'write_log'


def create(channel: str, event: str, data: Dict):
    return (
        Letter
        .objects
        .create(channel=channel, event=event, data=data)
    )

def complite(letter: Letter):
    exc_type, exc_value, exc_tb = sys.exc_info()
    if exc_tb:
        letter.status = LETTER_STATUSES.failed
        write_log(exc_type, exc_value, exc_tb, letter=letter)
    else:
        letter.status = LETTER_STATUSES.sent
    letter.save(update_fields=['status'])

def write_log(exc_type, exc_value, exc_tb, letter: Letter = None):
    Log.objects.create(
        letter=letter,
        title=str(exc_value),
        traceback=traceback.format_exc()
    )
