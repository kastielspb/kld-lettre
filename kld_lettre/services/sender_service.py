from typing import List, Tuple, Union
from django.db.models.query import QuerySet

from django.utils.module_loading import import_string

from ..const import settings
from ..globals import message_backends
from ..models import Letter

__all__ = 'get_backend', 'send', 'resend'


def initialize_backend(channel: str):
    config = settings.BACKENDS.get(channel)
    if config is None:
        raise KeyError(f'No backend provided for channel "{channel}"')

    backend = import_string(config.backend)
    message_backends[channel] = backend(**config.kwargs)
    return message_backends[channel]

def get_backend(channel: str):
    backend = message_backends.get(channel)
    if backend is None:
        backend = initialize_backend(channel=channel)
    return backend

def send(
    *,
    channels: Union[List[str], Tuple[str]],
    language: str = None,
    **kwargs
):
    '''
    Entrypoint to send message to channels.

    Arguments:
        channels (Union[List[str], Tuple[str]]): array of channels where \
        needs to send event
        language (`str`): language which will rendered letter
        **kwargs (dict): params for all passed channels
    '''
    result = []
    for channel in channels:
        backend = get_backend(channel=channel)
        if not backend:
            continue

        result.append(
            backend
            .send_letter(channel=channel, language=language, **kwargs)
        )
    return result

def resend(*, letters: QuerySet[Letter]):
    result = {
        'sent_count': 0,
        'error_ids': [],
    }
    for letter in letters:
        backend = get_backend(channel=letter.channel)
        if not backend:
            continue

        is_sent, letter = backend.send_letter(
            channel=letter.channel,
            event=letter.event,
            language=letter.data.get('language'),
            context={},
            recipients=[],
            letter=letter,
            fail_silently=True,
        )
        if is_sent:
            result['sent_count'] += 1
        else:
            result['error_ids'].append(letter.id)
    return result
