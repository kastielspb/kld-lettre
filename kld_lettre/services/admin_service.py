from copy import deepcopy

from ..const import settings
from ..configurators.admin import AdminConfigurator
from ..globals import admin_configs
from . import sender_service

__all__ = 'get_configurator',


def initialize_configurator(channel: str) -> AdminConfigurator:
    settings_config = settings.BACKENDS.get(channel)
    backend = sender_service.get_backend(channel=channel)
    config = {
        'config_form_path': (
            settings_config.kwargs.get('config_form_path')
            or backend.config_form_path
        ),
        'template_form_path': (
            settings_config.kwargs.get('template_form_path')
            or backend.template_form_path
        ),
    }
    admin_configs[channel] = AdminConfigurator(**config)
    return admin_configs[channel]

def get_configurator(channel: str) -> AdminConfigurator:
    configurator = admin_configs.get(channel)
    if configurator is None:
        configurator = initialize_configurator(channel=channel)
    return configurator
