from django import forms
from django.forms import fields, widgets
from django.utils.translation import pgettext_lazy

from entangled.forms import EntangledModelForm

from .models import ChannelConfig, Template
from .services import sender_service, template_service


__all__ = (
    'AddTemplateForm',
    'BaseConfigForm',
    'BaseTemplateForm',
    'EmailTemplateForm',
)


class AddTemplateForm(forms.ModelForm):

    class Meta:
        fields = 'channel', 'event'
        model = Template


class AddConfigForm(forms.ModelForm):

    class Meta:
        fields = 'channel',
        model = ChannelConfig


class BaseConfigForm(EntangledModelForm):
    """
    Base form that provides pretty visual displaying `backend` configs and
    saving it to JSON field. For more details how it works read here:
    https://github.com/jrief/django-entangled.

    Write fields that will needs to configure sending backend.
    """
    email_from = forms.EmailField()

    class Meta:
        entangled_fields = {'data': ['email_from']}
        model = ChannelConfig
        untangled_fields = ['channel']


class BaseTemplateForm(EntangledModelForm):
    """
    Base form that provides pretty visual displaying template data and saving
    it to JSON field. For more details how it works read here:
    https://github.com/jrief/django-entangled.

    Write fields that will needs on template rendering step.
    """

    class Meta:
        entangled_fields = {'data': []}
        model = Template
        untangled_fields = ['channel', 'event']

    def get_backend(self):
        return sender_service.get_backend(channel=self.instance.channel)

    def get_event(self, data):
        return data['event']

    def clean(self):
        data = super().clean()
        if self.instance:
            backend = self.get_backend()
            context = template_service.get_context_definition(
                event=self.get_event(data)
            )
            result, errors = template_service.render_template(
                template=data,
                context=context,
                fields=backend.get_template_fields(),
                renderer=backend.template_renderer,
            )
            if errors:
                raise forms.ValidationError(errors)
        return data


class EmailTemplateForm(BaseTemplateForm):
    """
    Email form that provides pretty visual displaying template data and saving
    it to JSON field. For more details how it works read here:
    https://github.com/jrief/django-entangled.
    """

    subject = fields.CharField(
        label=pgettext_lazy('kld_lettre', 'Subject'),
        widget=widgets.TextInput()
    )
    content = fields.CharField(
        label=pgettext_lazy('kld_lettre', 'Content'),
        widget=widgets.Textarea()
    )
    plain_text = fields.CharField(
        initial='',
        label=pgettext_lazy('kld_lettre', 'Plain text'),
        required=False,
        widget=widgets.Textarea(),
    )

    class Meta(BaseTemplateForm.Meta):
        entangled_fields = {'data': ['subject', 'content', 'plain_text']}
