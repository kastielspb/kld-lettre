from django.apps import AppConfig
from django.utils.translation import pgettext_lazy


class LettreConfig(AppConfig):
    name = 'kld_lettre'
    verbose_name = pgettext_lazy("kld_lettre", "Lettre")

    # def ready(self):
    #     from .discover import autodiscover
    #     autodiscover()

