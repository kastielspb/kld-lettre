# Generated by Django 3.2.6 on 2021-08-06 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kld_lettre', '0005_auto_20210805_1512'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChannelConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Updated')),
                ('channel', models.CharField(choices=[('email', 'Email'), ('sendpulse', 'Sendpulse')], max_length=255, unique=True, verbose_name='Channel')),
                ('data', models.JSONField(default=dict, verbose_name='Data')),
            ],
            options={
                'verbose_name': 'Channel config',
                'verbose_name_plural': 'Channel configs',
            },
        ),
        migrations.AlterModelOptions(
            name='letter',
            options={'verbose_name': 'Letter', 'verbose_name_plural': 'Letters'},
        ),
        migrations.AlterModelOptions(
            name='log',
            options={'ordering': ('-id',), 'verbose_name': 'Log', 'verbose_name_plural': 'Logs'},
        ),
        migrations.AlterModelOptions(
            name='template',
            options={'verbose_name': 'Template', 'verbose_name_plural': 'Templates'},
        ),
        migrations.AlterField(
            model_name='templatetranslation',
            name='language',
            field=models.CharField(choices=[('en', 'English'), ('ru', 'Russian')], default='en', max_length=32, verbose_name='Language'),
        ),
    ]
