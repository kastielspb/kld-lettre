from typing import Any, Dict

from django.template import Context, Template
from django.utils.translation import get_language, override

__all__ = 'Jinja2Renderer',


class Jinja2Renderer:

    def __call__(
        self,
        text: str,
        context: Dict[str, Any],
        language: str = None,
    ) -> str:
        if language is None:
            language = get_language()

        with override(language=language):
            return Template(text).render(Context(context))
