from typing import Optional

from django.utils.module_loading import import_string

__all__ = 'AdminConfigurator',


class AdminConfigurator:
    config_form_path: Optional[str] = 'kld_lettre.forms.BaseConfigForm'
    template_form_path: Optional[str] = 'kld_lettre.forms.BaseTemplateForm'

    def __init__(self, **kwargs):
        config_form_path = kwargs.get('config_form_path')
        if config_form_path:
            self.config_form_path = config_form_path

        template_form_path = kwargs.get('template_form_path')
        if template_form_path:
            self.template_form_path = template_form_path

    @property
    def config_form_class(self):
        return import_string(self.config_form_path)

    @property
    def template_form_class(self):
        return import_string(self.template_form_path)
