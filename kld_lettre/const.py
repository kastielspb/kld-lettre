from dataclasses import dataclass, field
from typing import Any, Dict, Iterable, List, Optional, Tuple, Union

from django.db.models.enums import IntegerChoices
from django.utils.translation import pgettext_lazy

from px_settings.contrib.django import settings as setting_wrap

__all__ = (
    'LETTER_STATUSES',
    'Settings',
    'settings',
    'settings_prefix',
)

settings_prefix = 'KLD_LETTRE'


class LETTER_STATUSES(IntegerChoices):
    draft = 1, pgettext_lazy('kld_lettre', 'Draft')
    sent = 2, pgettext_lazy('kld_lettre', 'Sent')
    failed = 3, pgettext_lazy('kld_lettre', 'Failed')


@dataclass
class BackendConfig:
    backend: str
    kwargs: Optional[Dict] = field(default_factory=dict)


@setting_wrap(settings_prefix)
@dataclass
class Settings:
    CHANNELS: Iterable
    EVENTS: Iterable
    EVENTS_CONTEXT: Dict[str, Dict]
    CHANNEL_EVENTS: Dict[str, Union[List[str], Tuple[str]]]
    BACKENDS: Dict[str, Union[str, Tuple[str, Dict[str, Any]]]]
    GENERATOR_RENDERERS: Optional[Dict[str, str]] = field(
        default_factory=dict
    )
    CHANNELS_CHOICES: Tuple[str, Any] = field(init=False)
    EVENT_CHOICES: Tuple[str, Any] = field(init=False)

    def __post_init__(self):
        self.CHANNELS_CHOICES = self.to_choices(self.CHANNELS)
        self.EVENT_CHOICES = self.to_choices(self.EVENTS)

        object.__setattr__(
            self,
            'BACKENDS',
            self.init_backends()
        )

    @staticmethod
    def to_choices(data):
        return getattr(data, 'choices', data)

    def init_backends(self) -> Dict[str, Any]:
        return {
            key: BackendConfig(**(
                {'backend': value}
                if isinstance(value, str)
                else {
                    'backend': value[0],
                    'kwargs': value[1]
                }
            ))
            for key, value in self.BACKENDS.items()
        }


settings = Settings()
