from template_messages.backends.base import BaseMessageBackend


class ConsolePrinterMessageBackend(BaseMessageBackend):

    def send(self, **kwargs):
        print('Send by ConsolePrinterMessageBackend')
        print('Kwargs', kwargs)
