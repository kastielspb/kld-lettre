from django.urls import re_path

from .views import *

urlpatterns = [
    re_path(r'^$', SomePageView.as_view(), name='some_page'),
]
