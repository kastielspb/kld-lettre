from django.contrib.auth import get_user_model
from django.views.generic import TemplateView

from kld_lettre.const import settings
from kld_lettre.services import sender_service

__all__ = 'SomePageView',

User = get_user_model()


class SomePageView(TemplateView):
    template_name = 'index.jinja'

    def get_context_data(self, **kwargs):
        sender_service.send(
            channels=[
                settings.CHANNELS.email,
            ],
            event=settings.EVENTS.event1,
            context={
                'var1': 'Val 1',
                'var2': 'Val 2',
            },
            recipients=[{'email': 'some@mail.com'}],
            language='ru',
        )
        return super().get_context_data(**kwargs)
