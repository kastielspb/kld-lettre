import os
from setuptools import setup, find_packages

from kld_lettre import __version__


base_dir = os.path.dirname(__file__)
with open(os.path.join(base_dir, "README.rst")) as f:
    long_description = f.read()

setup(
    name='kld-lettre',
    version=__version__,
    description='Django module to send letters from different channels.',
    long_description=long_description,
    long_description_content_type="text/x-rst",
    author='Maxim Shaitanov',
    author_email='maximshaitanov@gmail.com',
    url='https://gitlab.com/kastielspb/kld-lettre.git',
    packages=find_packages(exclude=['pilot', 'tests']),
    include_package_data=True,
    license='MIT',
    install_requires=['django>=3.0'],
    classifiers=[
        'Environment :: Web Environment',
        "Operating System :: OS Independent",
        'Intended Audience :: Developers',
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ]
)
